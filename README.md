<h2> Hey there! I'm Nothixal.</h2>

<h3>&nbsp;About Me </h3>

- 🤔 &nbsp; Exploring new technologies and developing software solutions and quick hacks.
- 🎓 &nbsp; Associates in Computer Science.
- 💼 &nbsp; Working as a Business Development Associate at VirtuBox InfoTech Private Limited.
- 🌱 &nbsp; Learning more about Cloud Architecture, Systems Design and Artificial Intelligence.
- ✍️ &nbsp; Pursuing Graphic Design and Blog Writing as hobbies/side hustles.

<a href="https://github.com/Nothixal">
  <img height="180em" src="https://gitlab-readme-stats.vercel.app/api?username=Nothixal&theme=buefy&show_icons=true" />
  <!-- <img height="180em" src="https://gitlab-readme-stats.vercel.app/api/top-langs/?username=Nothixal&theme=buefy&layout=compact" /> -->
</a>

⭐️ From [Nothixal](https://github.com/Nothixal)
